/*
 * DO NOT ALTER OR REMOVE COPYRIGHT NOTICES OR THIS HEADER.
 * 
 * Copyright 1997-2009 Sun Microsystems, Inc. All rights reserved.
 * 
 * The contents of this file are subject to the terms of either the GNU
 * General Public License Version 2 only ("GPL") or the Common
 * Development and Distribution License("CDDL") (collectively, the
 * "License"). You may not use this file except in compliance with the
 * License. You can obtain a copy of the License at
 * http://www.netbeans.org/cddl-gplv2.html
 * or nbbuild/licenses/CDDL-GPL-2-CP. See the License for the
 * specific language governing permissions and limitations under the
 * License. When distributing the software, include this License Header
 * Notice in each file and include the License file at 
 * nbbuild/licenses/CDDL-GPL-2-CP. Sun designates this 
 * particular file as subject to the "Classpath" exception as provided
 * by Sun in the GPL Version 2 section of the License file that
 * accompanied this code. If applicable, add the following below the
 * License Header, with the fields enclosed by brackets [] replaced by
 * your own identifying information:
 * "Portions Copyrighted [year] [name of copyright owner]"
 * 
 * Contributor(s):
 * 
 * The Original Software is NetBeans. The Initial Developer of the Original
 * Software is Sun Microsystems, Inc. Portions Copryright 1997-2006 Sun
 * Microsystems, Inc. All rights reserved.
 * 
 * If you wish your version of this file to be governed by only the CDDL
 * or only the GPL Version 2, indicate your decision by adding
 * "[Contributor] elects to include this software in this distribution
 * under the [CDDL or GPL Version 2] license." If you do not indicate a
 * single choice of license, a recipient has the option to distribute
 * your version of this file under either the CDDL, the GPL Version 2 or
 * to extend the choice of license to its licensees as provided above.
 * However, if you add GPL Version 2 code and therefore, elected the GPL
 * Version 2 licese, then the option applies only if the new code is
 * made subject to such option by the copyright holder.
 */
package org.netbeans.validation.api;

import java.util.Iterator;

/**
 *
 * @author Sean Carrick &lt;sean at gs-unitedlabs dot com&gt;
 *
 * @version 1.0
 * @since 1.0
 */
public class InvalidInputException extends IllegalArgumentException
        implements Iterable<Problem> {

    private final Problems problems;

    public InvalidInputException(Problems problems) {
        this.problems = problems;
    }

    public InvalidInputException(String msg, Problems problems) {
        super(msg);
        this.problems = problems;
    }

    public InvalidInputException(String msg, Validating v) {
        this(msg, v.getProblems());
    }

    public InvalidInputException(Validating v) {
        this(v.getProblems());
    }

    public static void throwIfNecessary(Validating v) throws InvalidInputException {
        throwIfNecessary(null, v);
    }

    public static void throwIfNecessary(String msg, Validating v) {
        if (v != null) {
            if (!v.isValid()) {
                Problems p = v.getProblems();
                if (p == null) {
                    throw new IllegalStateException(v + " ("
                            + v.getClass().getSimpleName() + ") reports itself "
                            + "invalid but returns null from getProblems(): " + v);
                }
                if (p.hasFatal()) {
                    if (msg == null) {
                        throw new InvalidInputException(v);
                    } else {
                        throw new InvalidInputException(msg, v);
                    }
                }
            }
        }
    }
    
    @Override
    public String getMessage() {
        String msg = super.getMessage();
        if (msg != null) {
            msg += ": " + problems.toString();
        } else {
            msg = problems.toString();
        }
        
        return msg;
    }
    
    /**
     * Get an object which lists all of the problems encountered with the input,
     * with severity and localized message.
     * 
     * @return 
     */
    public Problems getProblems() {
        return problems;
    }

    @Override
    public Iterator<Problem> iterator() {
        return problems.iterator();
    }

}
