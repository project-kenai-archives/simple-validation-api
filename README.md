# Simple Validation API

An archive of Tim Boudreau's Simple Validation API for Swing components.

## From the Simple Validation Project Main Wiki Page

This page is a place for ideas and thoughts about how this library should evolve.

### Validation Messages

The built-in validators, which provide a lot of the value of this library use fixed (but localized) validation messages - they cannot be changed. On the one hand, having these fixed validation messages for the built-in validators reduces complexity. Usually the messages are clear enough that you can just use them and not think too much about it. If someone desperately wanted to change messages, they could rewrite the resource bundle file and place it ahead of the standard one on the classpath. Or write a custom validator that wrappers the original, and translates the message based on pattern matching (though this would be fragile if messages are changed).

Some formalism for doing this seems needed, but it should also not add tremendous complexity - most of the time, people will just want to see and use strings, so we shouldn't either bloat the API or force people to use too many classes to do the simple things - i.e. the default usage inside a validator should probably still be 

```java
problems.add("Bad!!");
```

One bright note on this is that all of the built-in validators which produce messages are enum constants on the `Validators` class. Some validators can produce more than one error message.

So the way I think this should work is to have a comparable Enum, `Messages`, with names that match the `Validators` enum constants, and (perhaps?) a `Message` object which is, under the hood, what a `Problem` actually contains. i.e. 

```java
public enum Messages {
    CHARACTER_SET_NAME-invalid,
    CHARACTER_SET_NAME-unsupported,
}
```

or something like this (not sure if the name munging to ID cases where one validator can produce multiple messages is a good or bad idea at the moment)

So then there would be some sort of default `MessageProvider` that creates messages; and you could also pass (at the validation group level? or the validator level itself?) a wrapper one which could fall back to the default for things it does not want to change, but could completely control the message - and could be told what validator is actually talking to it.

The main questions, it seems to me are how do this without the API size getting out of control, and what the right level (probably not global) for injecting the thing which could intercept and rewrite messages is. 

### Component Name Use

Component names are sometimes used for other things; also, it may take some getting used to for people to think about assigning localized strings to them. It is an open question if we should find some other way to handle component names (there already is the client property option - see `ValidationListener.setComponentName()` and `ValidationListener.getComponentName()`). However, we need to do so in a way that keeps validators stateless and is not likely to encourage memory leaks in client code.

### Asynchronous Validation

There are cases where validation can be very expensive (network I/O, etc.). It would be possible to do something like this. The solution would work as follows:

* Need a marker interface or separate Validator subclass which identifies itself as asynchronous
* Need an interface that allows the validation group to disable the entire user interface while validation runs
* The UI (ValidationGroup.createErrorLabel()?) would need to show a progress bar or animated icon or similar
* The ValidationGroup collects the input on the event thread, then disables the UI. While the UI is disabled, it starts a progress indicator, runs the validation on a background thread, then re-enables it and updates the error feedback
* Probably this approach should prohibit using ValidationStrategy.ON_CHANGE, since disabling a UI for possibly several seconds whenever the user types a character is not polite.
* Any such solution should not complicate the API for people who don't need asynchronous validation - it should be able to be completely ignored by mainstream users

## Support
Since this library is rather old (from JDK 1.6 timeframe) and hasn't been in active development since at least 2009, support will be limited to whatever anyone wants to provide to others. Perhaps in the future, if interest in this project picks up, some sort of support (other than community volunteers) could possibly be set up.

## Roadmap
The first release will occur as soon as the entire API has been recreated, with some example usage project(s). After that, releases will only occur on major updates to the API itself. If the example projects are updated, a release will not be triggered.

## Contributing
Any and all contributions to this project are welcome!

If you know Java programming and want to tackle possibly adding new features, or updating existing features to a version of the JDK not newer than JDK 11 (for now - 2022), you may feel free to fork this repository and start working.

If you are not comfortable with API design work, but are capable of using an API in a project, feel free to use this API to create example projects that can be added to the examples that we will be recreating from the original project in the near future. Along these lines, you could contribute by simply using the API in your own projects and submitting bug reports, requests for enhancements, etc., as your way of contributing to this project.

There are a few internationalization bundle files contained within this API, for English, French, Italian, and German. If you know another language and would like to create a translation of these bundles for this project, we would greatly appreciate it.

If you are a technical writer, we would love your contribution to this project by creating Wiki pages for documenting how to use this API library in Java/Swing projects. Creating complete project tutorials in the Wiki section would be awesome!

## Authors and acknowledgment
The original authors of this library are:

* Tim Boudreau
* Hugo Heden

The current maintainer of this project is:

* Sean Carrick (GS United Labs and PekinSOFT Systems)

## License
The source code of this library is copyright 1997-2009 Sun Microsystems, Inc., and it is released under either the GNU General Public License (GPL) version 2.0 or the Common Development and Distribution License (CDDL). Read the license header at the top of each source code file for more information.

## Project status
This project is currently in its infancy, as it is taking time to recreate the Simple Validation API since the orignial source code repositories were taken off line by Oracle, Inc. The original project was hosted on the Project Kenai website at http://kenai.com/projects/simplevalidation/. Since that web site is now defunct, and the sources could not be located in any other manner, I am currently re-typing the entire project by viewing the source code files in the archived web site on the Wayback Machine.
